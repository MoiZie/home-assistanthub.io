<!doctype html>
  <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
  <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
  <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
  <!--[if gt IE 8]><!--> <html> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>MQTT HVAC - Home Assistant</title>
    <meta name="author" content="Home Assistant">
    <meta name="description" content="Instructions how to integrate MQTT HVAC into Home Assistant.">
    <meta name="viewport" content="width=device-width">
    <link rel="canonical" href="https://home-assistant.io/components/climate.mqtt/">
    <meta property="fb:app_id" content="338291289691179">
    <meta property="og:title" content="MQTT HVAC">
    <meta property="og:site_name" content="Home Assistant">
    <meta property="og:url" content="https://home-assistant.io/components/climate.mqtt/">
    <meta property="og:type" content="article">
    <meta property="og:description" content="Instructions how to integrate MQTT HVAC into Home Assistant.">
    <meta property="og:image" content="https://home-assistant.io/images/default-social.png">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@home_assistant">
    <meta name="twitter:title" content="MQTT HVAC">
    <meta name="twitter:description" content="Instructions how to integrate MQTT HVAC into Home Assistant.">
    <meta name="twitter:image" content="https://home-assistant.io/images/default-social.png">
    <link href="/stylesheets/screen.css" media="screen, projection, print" rel="stylesheet">
    <link href="/atom.xml" rel="alternate" title="Home Assistant" type="application/atom+xml">
    <link rel='shortcut icon' href='/images/favicon.ico' />
    <link rel='icon' type='image/png' href='/images/favicon-192x192.png' sizes='192x192' />
  </head>
  <body >
    <header class='site-header'>
      <div class="grid-wrapper">
  <div class="grid">
    <div class="grid__item three-tenths lap-two-sixths palm-one-whole ha-title">
  <a href="/" class="site-title">
    <img width='40' src='/demo/favicon-192x192.png'>
    <span>Home Assistant</span>
  </a>
</div>
<div class="grid__item seven-tenths lap-four-sixths palm-one-whole">
  <nav>
    <input type="checkbox" id="toggle">
<label for="toggle" class="toggle" data-open="Main Menu" data-close="Close Menu"></label>
<ul class="menu pull-right">
  <li><a href="/getting-started/">Getting started</a></li>
  <li><a href="/components/">Components</a></li>
  <li><a href="/docs/">Docs</a></li>
  <li><a href="/cookbook/">Examples</a></li>
  <li><a href="/developers/">Developers</a></li>
  <li><a href="/blog/">Blog</a></li>
  <li><a href="/help/">Need help?</a></li>
  <li><a href='#' class='show-search'><i class="icon-search"></i></a></li>
</ul>
  </nav>
  <div class='search-container' style='display: none'>
    <div class='search'>
      <i class="icon-search"></i>
      <input id='search' placeholder='Search the docs…'>
      <a href='#' class='close'><i class="icon-remove-sign"></i></a>
    </div>
  </div>
</div>
  </div>
</div>
    </header>
    <div class="grid-wrapper">
      <div class="grid grid-center">
        <div class="grid__item two-thirds lap-one-whole palm-one-whole">
          <article class="page">
  <header>
    <h1 class="title indent">
      MQTT HVAC
    </h1>
  </header>
  <hr class="divider">
  <p>The <code class="highlighter-rouge">mqtt</code> climate platform lets you control your MQTT enabled HVAC devices.</p>
<p>The platform currently works in optimistic mode, which means it does not obtain states from MQTT topics, but it sends and remembers control commands.</p>
<p>It uses a sensor under the hood to obtain the current temperature.</p>
<div class="language-yaml highlighter-rouge"><pre class="highlight"><code><span class="c1"># Example configuration.yaml entry</span>
<span class="s">climate</span><span class="pi">:</span>
  <span class="pi">-</span> <span class="s">platform</span><span class="pi">:</span> <span class="s">mqtt</span>
    <span class="s">name</span><span class="pi">:</span> <span class="s">Study</span>
    <span class="s">current_temperature_topic</span><span class="pi">:</span> <span class="s">/sensors/hvac_study/current_temp</span>
    <span class="s">temperature_command_topic</span><span class="pi">:</span> <span class="s">/sensors/hvac_study/target_temp</span>
</code></pre>
</div>
<p>Configuration variables <em>except</em> for MQTT topics:</p>
<ul>
  <li><strong>name</strong> (<em>Required</em>): Name of MQTT HVAC.</li>
  <li><strong>qos</strong> (<em>Optional</em>): The maximum QoS level of the state topic. Default is <code class="highlighter-rouge">0</code> and will also be used to publishing messages.</li>
  <li><strong>retain</strong> (<em>Optional</em>): If the published message should have the retain flag on or not.</li>
  <li><strong>send_if_off</strong> (<em>Optional</em>): Set to <code class="highlighter-rouge">false</code> to suppress sending of all MQTT messages when the current mode is <code class="highlighter-rouge">Off</code>. Defaults to <code class="highlighter-rouge">true</code>.</li>
  <li><strong>initial</strong> (<em>Optional</em>): Set the initial target temperature. Defaults to 21 degrees.</li>
  <li><strong>payload_on</strong> (<em>Optional</em>): For MQTT topics that control an <code class="highlighter-rouge">on</code> / <code class="highlighter-rouge">off</code> value (e.g., <code class="highlighter-rouge">aux_command_topic</code>), set the value that should be sent for <code class="highlighter-rouge">on</code>. Defaults to ‘ON’.</li>
  <li><strong>payload_off</strong> (<em>Optional</em>): For MQTT topics that control an <code class="highlighter-rouge">on</code> / <code class="highlighter-rouge">off</code> value (e.g., <code class="highlighter-rouge">aux_command_topic</code>), set the value that should be sent for <code class="highlighter-rouge">off</code>. Defaults to ‘OFF’.</li>
</ul>
<p>Configuration of the MQTT topics:</p>
<ul>
  <li><strong>current_temperature_topic</strong> (<em>Optional</em>): The MQTT topic on which to listen for the current temperature</li>
  <li><strong>power_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the power state. This is useful if your device has a separate power toggle in addition to mode.</li>
  <li><strong>mode_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the HVAC operation mode.</li>
  <li><strong>mode_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the HVAC operation mode. If this is not set, the operation mode works in optimistic mode (see below).</li>
  <li><strong>temperature_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the target temperature.</li>
  <li><strong>temperature_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes in the target temperature. If this is not set, the target temperature works in optimistic mode (see below).</li>
  <li><strong>fan_mode_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the fan mode.</li>
  <li><strong>fan_mode_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the HVAC fan mode. If this is not set, the fan mode works in optimistic mode (see below).</li>
  <li><strong>swing_mode_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the swing mode.</li>
  <li><strong>swing_mode_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the HVAC swing mode. If this is not set, the swing mode works in optimistic mode (see below).</li>
  <li><strong>away_mode_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the away mode.</li>
  <li><strong>away_mode_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the HVAC away mode. If this is not set, the away mode works in optimistic mode (see below).</li>
  <li><strong>hold_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to change the hold mode.</li>
  <li><strong>hold_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the HVAC hold mode. If this is not set, the hold mode works in optimistic mode (see below).</li>
  <li><strong>aux_command_topic</strong> (<em>Optional</em>): The MQTT topic to publish commands to switch auxiliary heat.</li>
  <li><strong>aux_state_topic</strong> (<em>Optional</em>): The MQTT topic to subscribe for changes of the auxiliary heat mode. If this is not set, the auxiliary heat mode works in optimistic mode (see below).</li>
</ul>
<h4>Optimistic mode</h4>
<p>If a property works in <em>optimistic mode</em> (when the corresponding state topic is not set), home assistant will assume that any state changes published to the command topics did work and change the internal state of the entity immediately after publishing to the command topic. If it does not work in optimistic mode, the internal state of the entity is only updated when the requested update is confirmed by the device through the state topic.</p>
<h3><a class="title-link" name="example" href="#example"></a> Example</h3>
<p>A full configuration example looks like the one below.</p>
<div class="language-yaml highlighter-rouge"><pre class="highlight"><code><span class="c1"># Full example configuration.yaml entry</span>
<span class="s">climate</span><span class="pi">:</span>
  <span class="pi">-</span> <span class="s">platform</span><span class="pi">:</span> <span class="s">mqtt</span>
    <span class="s">name</span><span class="pi">:</span> <span class="s">Study</span>
    <span class="s">modes</span><span class="pi">:</span>
      <span class="pi">-</span> <span class="s">off</span>
      <span class="pi">-</span> <span class="s">cool</span>
      <span class="pi">-</span> <span class="s">fan_only</span>
    <span class="s">swing_modes</span><span class="pi">:</span>
      <span class="pi">-</span> <span class="s">on</span>
      <span class="pi">-</span> <span class="s">off</span>
    <span class="s">fan_modes</span><span class="pi">:</span>
      <span class="pi">-</span> <span class="s">high</span>
      <span class="pi">-</span> <span class="s">medium</span>
      <span class="pi">-</span> <span class="s">low</span>
    <span class="s">power_command_topic</span><span class="pi">:</span> <span class="s2">"</span><span class="s">study/ac/power/set"</span>
    <span class="s">mode_command_topic</span><span class="pi">:</span> <span class="s2">"</span><span class="s">study/ac/mode/set"</span>
    <span class="s">temperature_command_topic</span><span class="pi">:</span> <span class="s2">"</span><span class="s">study/ac/temperature/set"</span>
    <span class="s">fan_mode_command_topic</span><span class="pi">:</span> <span class="s2">"</span><span class="s">study/ac/fan/set"</span>
    <span class="s">swing_mode_command_topic</span><span class="pi">:</span> <span class="s2">"</span><span class="s">study/ac/swing/set"</span>
</code></pre>
</div>
</article>
        </div>
        <aside id="sidebar" class="grid__item one-third lap-one-whole palm-one-whole">
          <div class="grid">
    <section class="aside-module grid__item one-whole lap-one-half">
  <div class='edit-github'><a href='https://github.com/home-assistant/home-assistant.github.io/tree/current/source/_components/climate.mqtt.markdown'>Edit this page on GitHub</a></div>
  <div class='brand-logo-container section'>
      <img src='/images/supported_brands/heat-control.png' />
  </div>
    <div class='section'>
      IoT class<sup><a href='/blog/2016/02/12/classifying-the-internet-of-things/#classifiers'><i class="icon-info-sign"></i></a></sup>: Local Polling
    </div>
    <div class='section'>
      Introduced in release: 0.55
    </div>
    <div class='section'>
      Source:
      <a href='https://github.com/home-assistant/home-assistant/blob/master/homeassistant/components/climate/mqtt.py'>climate/mqtt.py</a>
    </div>
    <div class='section'>
      This is a platform for
      <a href='/components/climate/'>the Climate component</a>.
    </div>
          <div class='section'>
          <h1 class='title delta'>Related components</h1>
          <ul class='divided'>
        <li><a href='/components/mqtt/'>
          MQTT
        </a></li>
        <li><a href='/components/alarm_control_panel.mqtt/'>
          MQTT Alarm Control Panel
        </a></li>
        <li><a href='/components/binary_sensor.mqtt/'>
          MQTT Binary Sensor
        </a></li>
        <li><a href='/components/camera.mqtt/'>
          MQTT Camera
        </a></li>
        <li><a href='/components/cover.mqtt/'>
          MQTT Cover
        </a></li>
        <li><a href='/components/device_tracker.mqtt/'>
          MQTT Device Tracker
        </a></li>
        <li><a href='/components/fan.mqtt/'>
          MQTT Fan
        </a></li>
        <li><a href='/components/light.mqtt/'>
          MQTT Light
        </a></li>
        <li><a href='/components/lock.mqtt/'>
          MQTT Lock
        </a></li>
        <li><a href='/components/notify.mqtt/'>
          MQTT Notifications
        </a></li>
        <li><a href='/components/sensor.mqtt/'>
          MQTT Sensor
        </a></li>
        <li><a href='/components/switch.mqtt/'>
          MQTT Switch
        </a></li>
        <li><a href='/components/vacuum.mqtt/'>
          MQTT Vacuum
        </a></li>
    </ul>
    </div>
    <div class='section'>
    <h1 class="title delta">Category Climate</h1>
    <ul class='divided'>
        <li>
            <a href='/components/climate.ephember/'>EPH Controls Ember Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.eq3btsmart/'>EQ3 Bluetooth Smart Thermostats</a>
        </li>
        <li>
            <a href='/components/climate.ecobee/'>Ecobee Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.flexit/'>Flexit A/C controller</a>
        </li>
        <li>
            <a href='/components/climate.generic_thermostat/'>Generic Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.heatmiser/'>Heatmiser Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.hive/'>Hive Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.homematic/'>Homematic Thermostats</a>
        </li>
        <li>
            <a href='/components/climate.honeywell/'>Honeywell Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.knx/'>KNX Climate</a>
        </li>
        <li>
            MQTT HVAC
        </li>
        <li>
            <a href='/components/climate.mysensors/'>MySensors HVAC</a>
        </li>
        <li>
            <a href='/components/climate.nest/'>Nest Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.netatmo/'>Netatmo Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.oem/'>OpenEnergyMonitor WiFi Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.proliphix/'>Proliphix Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.radiotherm/'>Radio Thermostat (3M Filtrete) Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.sensibo/'>Sensibo A/C controller</a>
        </li>
        <li>
            <a href='/components/climate.tado/'>Tado Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.tesla/'>Tesla HVAC</a>
        </li>
        <li>
            <a href='/components/climate.toon/'>Toon Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.vera/'>Vera Thermostat</a>
        </li>
        <li>
            <a href='/components/climate.wink/'>Wink Climate</a>
        </li>
        <li>
            <a href='/components/climate.zwave/'>Z-Wave Climate</a>
        </li>
        <li>
            <a href='/components/maxcube/'>eQ-3 MAX! Cube</a>
        </li>
        <li>
            <a href='/components/binary_sensor.maxcube/'>eQ-3 MAX! Cube binary sensors</a>
        </li>
        <li>
            <a href='/components/climate.maxcube/'>eQ-3 MAX! Cube thermostat</a>
        </li>
    </ul>
    </div>
</section>
</div>
        </aside>
      </div>
    </div>
    <footer>
      <div class="grid-wrapper">
  <div class="grid">
    <div class="grid__item">
      <div class="copyright">
  <a rel="me" href='https://twitter.com/home_assistant'><i class="icon-twitter"></i></a>
  <a rel="me" href='https://facebook.com/homeassistantio'><i class="icon-facebook"></i></a>
  <a rel="me" href='https://plus.google.com/110560654828510104551'><i class="icon-google-plus"></i></a>
  <a rel="me" href='https://github.com/home-assistant/home-assistant'><i class="icon-github"></i></a>
  <div class="credit">
    Contact us at <a href='mailto:hello@home-assistant.io'>hello@home-assistant.io</a> (no support!).<br>
    Website powered by <a href='http://jekyllrb.com/'>Jekyll</a> and the <a href='https://github.com/coogie/oscailte'>Oscalite theme</a>.<br />
    Hosted by <a href='https://pages.github.com/'>GitHub</a> and served by <a href='https://cloudflare.com'>CloudFlare</a>.
  </div>
  <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">home-assistant.io</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
</div>
    </div>
  </div>
</div>
    </footer>
    <script>
  var _gaq=[['_setAccount','UA-57927901-1'],['_trackPageview']];
  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.js"></script>
<script type="text/javascript">
docsearch({
  apiKey: 'ae96d94b201c5444c8a443093edf3efb',
  indexName: 'home-assistant',
  inputSelector: '#search',
  debug: false // Set debug to true if you want to inspect the dropdown
});
document.querySelector('.search .close').addEventListener('click', function(ev) {
  ev.preventDefault();
  document.querySelector('.search-container').style.display = 'none';
});
document.querySelector('.show-search').addEventListener('click', function(ev) {
  ev.preventDefault();
  document.querySelector('.search-container').style.display = 'block';
  document.getElementById('toggle').checked = false;
  document.querySelector('.search-container input').focus();
});
</script>
  </body>
</html>
